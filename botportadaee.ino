#include <WiFi.h>
#include <WiFiClientSecure.h>
#include "UniversalTelegramBot.h"
#include "esp_wpa2.h"
#include <Wire.h>
#include "msgs.h"
#include "secretes.h"	//Contem as senhas e chaves de token
#include  "ping.h"

int counter = 0;
WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

//Ping
IPAddress adr = IPAddress(8,8,8,8);

#define MAX_USER	5
String 
	id, 
	text,
	from_name,
	lastDoorOpen=Contato,
	lastDoorOpener="start";//Váriaveis para armazenamento do ID e TEXTO gerado pelo Usuario
String user[MAX_USER];
unsigned long tempo;
bool IamAlive = false;

//prototipos
bool checkWordList(String word,const String list[], int len);
bool UserVerify();
bool SaveUser(String newUser);
void ReadUsers();
void CheckDoor();
bool checkWordList(String word,const String list[], int len);
void sendIdea(String id,String name,const String list[], int len);
String joinPhrases(const String list[], int len,String separator);
void readTel();

#define PORTA	14
#define SENSOR	2

void setup()
{
	Serial.begin(115200);
	delay(10);

	pinMode(PORTA, OUTPUT);//PORTA conectado à saida
	digitalWrite(PORTA, LOW);
	
	pinMode(SENSOR, INPUT_PULLUP);//PORTA conectado à saida


	Serial.println();
	Serial.print("Connecting to network: ");
	Serial.println(ssid);
	WiFi.disconnect(true);  //disconnect form wifi to set new wifi connection
	WiFi.mode(WIFI_STA); //init wifi mode
	esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)EAP_ANONYMOUS_IDENTITY, strlen(EAP_ANONYMOUS_IDENTITY)); 
	esp_wifi_sta_wpa2_ent_set_username((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY));
	esp_wifi_sta_wpa2_ent_set_password((uint8_t *)EAP_PASSWORD, strlen(EAP_PASSWORD));
	esp_wpa2_config_t config = WPA2_CONFIG_INIT_DEFAULT(); //set config settings to default
	esp_wifi_sta_wpa2_ent_enable(&config); //set config settings to enable function
	WiFi.begin(ssid); //connect to wifi
	
	while (WiFi.status() != WL_CONNECTED)
	{
		Serial.print(".");
		delay(500);
		counter++;
		if(counter>=60){ //after 30 seconds timeout - reset board
			ESP.restart();
		}
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.println("IP address: ");
	Serial.println(WiFi.localIP());
	ReadUsers();

	delay(500);
	randomSeed(millis());
}
bool UserVerify()
{
	for (int i = 0; i < MAX_USER; i++)
	{
		if(id == user[i]) return true;
	}
	return false;
}
bool SaveUser(String newUser)
{
	static int index = 0;
	if(index > (MAX_USER - sizeof(Adms)/SZ) ) return false;

	user[index] = newUser;
	Serial.println(index+" "+newUser);
	index++;
	return true;
}
void ReadUsers()
{
	int len = sizeof(Adms)/SZ;
	for (int i = 0; i < len; i++)
	{
		user[MAX_USER-1-i]=Adms[i];
	}
}

void CheckDoor()
{
	static long int tempo = 0;
	static int lastTime = 0;
	static bool hold = true;
	bool sensor;
	
	sensor = digitalRead(SENSOR);
	if(sensor)
	{
		int temp;
		hold = true;
		temp = (millis() - tempo)/30000;
		if(lastTime!=temp)
		{
			lastTime=temp;
			Serial.print(temp*30);
			Serial.println(" segundos");
			switch(temp)
			{
				case 1:
				sendIdea(lastDoorOpen, lastDoorOpener, alert1, sizeof(alert1));
				break;
				
				case 2:
				sendIdea(lastDoorOpen, lastDoorOpener, alert2, sizeof(alert2));
				break;
				
				case 4:
				sendIdea(lastDoorOpen, lastDoorOpener, alert3, sizeof(alert3));
				break;
			}
		}
	}
	else 
	{
		if((millis() - tempo > 3000)&& hold)
		{
			hold = false;
			bot.sendMessage(lastDoorOpen,"a porta ficou "+String(int((millis() - tempo)/1000))+" segundos aberta","Markdown");
		}
		tempo = millis();
	}
}

void loop()
{
	if (millis() - tempo > 2000)//Faz a verificaçao das funçoes a cada 2 Segundos
	{
		readTel();//Funçao para ler o telegram
		tempo = millis();//Reseta o tempo
		if(!IamAlive)
		{
			sendIdea(Contato,"",despertar,sizeof(despertar));
			IamAlive = true;
		}
	}
	CheckDoor();
	checkInternet();
}
bool checkWordList(String word,const String list[], int len)
{
	len = len/SZ;
	for (int i = 0; i < len; i++)
	{
		if (word.indexOf(list[i]) > -1) return true;
	}
	return false;
}
void sendIdea(String id,String name,const String list[], int len)
{
	len = len/SZ;
	String temp = "";
	temp += name;
	temp += list[random(len)];
	bot.sendMessage(id,temp,"Markdown");
}
String joinPhrases(const String list[], int len,String separator)
{
	len = len/SZ;
	String temp = "";
	for (int i = 0; i < len; i++)
	{
		if(list[i] != "")
		{
			temp += list[i];
			temp += separator;
		}
	}
	return temp;
}

String convertUnicodePTBR(String msg)
{
	for(int letter=0xc0;letter<=0xff;letter++)
	{
		String latin = "00";
		latin += String(letter, HEX);
		int posicao = msg.indexOf(latin);
		if(posicao>-1)
		{
			String temp = "";
			for(int i=0; i<posicao-1;i++)
			{
				temp += msg[i];
			}
			latin = "";
			for(int i=posicao; i<posicao+4;i++)
			{
				latin += msg[i];
			}
			temp += lantinBasicChar[letter-0xC0];
			latin = "";
			for(int i=posicao+4; i<msg.length();i++)
			{
				temp += msg[i];
			}
			msg = temp;
		}
	}
	return msg;
}

void readTel()//Funçao que faz a leitura do Telegram.
{
	int newmsg = bot.getUpdates(bot.last_message_received + 1);

	for (int i = 0; i < newmsg; i++)//Caso haja X mensagens novas, fara este loop X Vezes.
	{
		id = bot.messages[i].chat_id;//Armazenara o ID do Usuario à Váriavel.
		text = bot.messages[i].text;//Armazenara o TEXTO do Usuario à Váriavel.
		//from_name = "@"+bot.messages[i].from_name+" "+bot.messages[i].from_lastName+", ";
		from_name = "@"+bot.messages[i].from_userName+" , ";
		from_name = convertUnicodePTBR(from_name);
		if (from_name == "@") from_name = "Guest";
		else
		{
			Serial.println("id");
			Serial.println(id);
			Serial.println("text");
			Serial.println(text);
			Serial.println("from_name");
			Serial.println(from_name);
			Serial.println("");
		}
		text.toUpperCase();//Converte a STRING_TEXT inteiramente em Maiuscúla.

		if(UserVerify())
		{

			// Filtro para mensagem em branco
			if (text == "") Serial.println("Mensagem vazia");

			// Comando de abrir a porta
			else if (text.indexOf(comandos2[0]) > -1)
			{
				sendIdea(id, from_name, portaAberta, sizeof(portaAberta));
				lastDoorOpen = id;
				lastDoorOpener = from_name;
			}
			// Lista os Adms
			else if (text.indexOf(comandos2[1]) > -1)
			{
				text = "```";
				text += joinPhrases(user,sizeof(user),"\n");
				text += "```";
				bot.sendMessage(id,text,"Markdown");//Envia uma mensagem com seu ID.
			}

			// Ajuda
			else if (text.indexOf(comandos2[2]) > -1)
			{
				bot.sendMessage(id,joinPhrases(helpMenu,sizeof(helpMenu),"\n"),"Markdown");//Envia uma mensagem com seu ID.
				bot.sendMessageWithInlineKeyboard(id, "Comandos","", joinPhrases(keyboardJson,sizeof(keyboardJson),""));
			}

			// Lista os desenvolvedores
			else if (text.indexOf(comandos2[3]) > -1)
				bot.sendMessage(id,joinPhrases(Devs,sizeof(Devs),"\n"),"Markdown");

			// Nome feio
			else if (checkWordList(text,badWords,sizeof(badWords)))
				sendIdea(id, from_name, badMsgs, sizeof(badMsgs));

			// Comandos de bar
			else if (checkWordList(text,barWords,sizeof(barWords)))
				sendIdea(id, from_name, barMsgs, sizeof(barMsgs));

			// comando inválido ou desconhecido
			else sendIdea(id, from_name, erro, sizeof(erro));
		}

		//Cadastra Adms
		else if(text == senha)
		{
			if(SaveUser(id))
				sendIdea(id, from_name, cadastrado, sizeof(cadastrado));
			else bot.sendMessage(id,"sorry","Markdown");
		}

		//Usuário não autorizado
		else sendIdea(id, from_name, desconhecido, sizeof(desconhecido));
	}
}
void checkInternet()
{
	static long delayTime,count;
	if (millis() - delayTime > 10000)//Faz a verificaçao das funçoes a cada 2 Segundos
	{
		delayTime = millis();//Reseta o delayTime
		if(count<360) {count++;return;}
		count = 0;
		
		Serial.println("Disparado o Ping para o google");
		if (ping_start(adr, 4, 0, 0, 5))
			Serial.println("OK");
		else
		{
			Serial.println("FAILED");
			ESP.restart();
		}
	}
}
